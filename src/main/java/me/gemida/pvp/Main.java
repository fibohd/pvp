package me.gemida.pvp;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import me.gemida.pvp.command.PvP_CMD;
import me.gemida.pvp.listener.PvPListener;

public class Main extends JavaPlugin{
	private static HashMap<UUID, Boolean> pvp = new HashMap<UUID, Boolean>();
	@Override
	public void onEnable() {
		getCommand("pvp").setExecutor(new PvP_CMD());
		Bukkit.getPluginManager().registerEvents(new PvPListener(), this);
	}
	
	public static HashMap<UUID, Boolean> getPvp() {
		return pvp;
	}
}
