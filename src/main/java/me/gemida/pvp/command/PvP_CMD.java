package me.gemida.pvp.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.gemida.pvp.Main;

public class PvP_CMD implements CommandExecutor{

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player p = (Player)sender;
		if(command.getName().equalsIgnoreCase("pvp")) {
			if(Main.getPvp().get(p.getUniqueId()) != null) {
				if(Main.getPvp().get(p.getUniqueId())) {
					Main.getPvp().put(p.getUniqueId(), false);
					p.sendMessage("§aDu bist nun nicht mehr angreifbar.");
				}else if(!Main.getPvp().get(p.getUniqueId())){
					Main.getPvp().put(p.getUniqueId(), true);
					p.sendMessage("§cDu bist nun angreifbar.");
				}
			}else {
				Main.getPvp().put(p.getUniqueId(), true);
				p.sendMessage("§cDu bist nun angreifbar.");
			}
		}
		return true;
	}

}
