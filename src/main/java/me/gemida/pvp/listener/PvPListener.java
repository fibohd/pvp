package me.gemida.pvp.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import me.gemida.pvp.Main;

public class PvPListener implements Listener {
	@EventHandler
	public static void onDamage(EntityDamageByEntityEvent e) {
		if(!Main.getPvp().get(e.getEntity().getUniqueId()) && e.getDamager() instanceof Player) {
			e.setCancelled(true);
		}else if(!Main.getPvp().get(e.getDamager().getUniqueId()) && e.getEntity() instanceof Player) {
			e.setCancelled(true);
		}
	}
}
